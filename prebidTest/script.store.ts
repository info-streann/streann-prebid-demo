interface Scripts {
    name: string;
    src: string;
}  
export const ScriptStore: Scripts[] = [
    {name: 'prebid', src: 'https://acdn.adnxs.com/prebid/not-for-prod/prebid.js'},
    {name: 'videojs', src: 'https://cdnjs.cloudflare.com/ajax/libs/video.js/6.4.0/video.js'},
    {name: 'videojsmin', src:'https://cdnjs.cloudflare.com/ajax/libs/videojs-vast-vpaid/2.0.2/videojs_5.vast.vpaid.min.js'}
];