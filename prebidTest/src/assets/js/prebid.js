var page_load_time;
    
    page_load_time = new Date().getTime() - performance.timing.navigationStart;
    console.log(page_load_time + "ms -- Player loading!");
    console.log("TUKA")
    var vid1 = videojs('vid1');

    page_load_time = new Date().getTime() - performance.timing.navigationStart;
    console.log(page_load_time + "ms -- Player loaded!");

    function invokeVideoPlayer(url) {

        page_load_time = new Date().getTime() - performance.timing.navigationStart;
        console.log(page_load_time + "ms -- Prebid VAST url = " + url);

        /* Access the player instance by calling `videojs()` and passing
           in the player's ID. Add a `ready` listener to make sure the
           player is ready before interacting with it. */

        videojs("vid1").ready(function() {

            page_load_time = new Date().getTime() - performance.timing.navigationStart;
            console.log(page_load_time + "ms -- Player is ready!");

            /* PASS SETTINGS TO VAST PLUGIN

               Pass in a JSON object to the player's `vastClient` (defined
               by the VAST/VPAID plugin we're using). The requires an
               `adTagUrl`, which will be the URL returned by Prebid. You
               can view all the options available for the `vastClient`
               here:

               https://github.com/MailOnline/videojs-vast-vpaid#options */

            var player = this;
            var vastAd = player.vastClient({
                adTagUrl: url,
                playAdAlways: true,
                verbosity: 4,
                vpaidFlashLoaderPath: "https://github.com/MailOnline/videojs-vast-vpaid/blob/RELEASE/bin/VPAIDFlash.swf?raw=true",
                autoplay: true
            });

            page_load_time = new Date().getTime() - performance.timing.navigationStart;
            console.log(page_load_time + "ms -- Prebid VAST tag inserted!");

            player.muted(true);
            player.play();

            page_load_time = new Date().getTime() - performance.timing.navigationStart;
            console.log(page_load_time + "ms -- invokeVideoPlayer complete!");

        });
    }   