import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdsTestComponent } from './ads-test/ads-test.component';
import { PrebidTestComponent } from './prebid-test/prebid-test.component';

const routes: Routes = [
  { path: 'home', component: PrebidTestComponent },
  {
    path: '', component: AdsTestComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
 }
