import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrebidTestComponent } from './prebid-test/prebid-test.component';
import { RouterModule } from '@angular/router';
import { AdsTestComponent } from './ads-test/ads-test.component';

@NgModule({
  declarations: [
    AppComponent,
    PrebidTestComponent,
    AdsTestComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
