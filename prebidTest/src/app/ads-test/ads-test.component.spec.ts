import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdsTestComponent } from './ads-test.component';

describe('AdsTestComponent', () => {
  let component: AdsTestComponent;
  let fixture: ComponentFixture<AdsTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdsTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdsTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
