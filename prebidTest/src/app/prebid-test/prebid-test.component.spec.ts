import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrebidTestComponent } from './prebid-test.component';

describe('PrebidTestComponent', () => {
  let component: PrebidTestComponent;
  let fixture: ComponentFixture<PrebidTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrebidTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrebidTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
